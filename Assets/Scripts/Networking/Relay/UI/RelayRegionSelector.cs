﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace MagesDungeon.Networking.Relay
{
    public class RelayRegionSelector : MonoBehaviour
    {
        [SerializeField] private TMP_Dropdown _regionsDropdown;
        [SerializeField] private Button _fetchRegionsButton;
        
        private RelayManager _relayManager;
        private readonly List<IDisposable> _disposables = new();

        private void Start()
        {
            _relayManager = NetworkingFacade.Instance.RelayManager;
            _disposables.Add(_relayManager.SelectedRegion.AsObservable().Subscribe());
            UpdateRegionOptions();
            _regionsDropdown.onValueChanged.AddListener(OnRegionSelected);
            _fetchRegionsButton.onClick.AddListener(OnFetchRegionsClicked);
        }

        private void OnDestroy()
        {
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
        }

        private void UpdateRegionOptions()
        {
            _regionsDropdown.options.Clear();
            var availableRegions = _relayManager.AvailableRegions;
            var isRegionsListValid = availableRegions?.Count > 0;
            if (isRegionsListValid)
            {
                foreach (var region in availableRegions)
                {
                    _regionsDropdown.options.Add(new TMP_Dropdown.OptionData(region.Id));
                }
            }
            
            var selectedRegionId = availableRegions?.IndexOf(_relayManager.SelectedRegion.Value) ?? -1;
            _regionsDropdown.SetValueWithoutNotify(selectedRegionId);
            _regionsDropdown.interactable = isRegionsListValid;
        }
        
        private void OnRegionSelected(int optionIndex)
        {
            _relayManager.SelectedRegion.Value = _relayManager.AvailableRegions[optionIndex];
        }
        
        private async void OnFetchRegionsClicked()
        {
            try
            {
                _fetchRegionsButton.interactable = false;
                await _relayManager.FetchAvailableRegionsAsync(this.GetCancellationTokenOnDestroy());
                UpdateRegionOptions();
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
            finally
            {
                _fetchRegionsButton.interactable = !_regionsDropdown.interactable;
            }
        }
    }
}