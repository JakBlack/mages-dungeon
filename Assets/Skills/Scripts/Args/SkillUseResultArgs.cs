﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    public struct SkillUseResultArgs
    {
        public bool IsSuccessful;
        public RaycastHit TargetHit;
    }
}