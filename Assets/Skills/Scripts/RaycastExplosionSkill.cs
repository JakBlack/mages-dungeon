﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    [CreateAssetMenu(menuName = "Player/Skills/Raycast/Explosion", fileName = nameof(RaycastExplosionSkill))]
    public class RaycastExplosionSkill : RaycastSkillBase
    {
        [SerializeField] private Rigidbody _spawnObject;
        [SerializeField] private float _spawnForce = 10;
        [SerializeField] private ForceMode _forceMode = ForceMode.VelocityChange;
        
        public override SkillUseResultArgs UseSkill(SkillUseArgs args)
        {
            var result = base.UseSkill(args);
            if (result.IsSuccessful)
            {
                SpawnExplosion(result.TargetHit, args.SpawnReferenceTransform, args.IgnoreColliders);
            }

            return result;
        }

        private void SpawnExplosion(RaycastHit hit, Transform refTransform, Collider[] ignoreColliders)
        {
            var spawnPos = refTransform.position;
            var forceDirection = (hit.point - spawnPos).normalized;
            var spawnedObject = Instantiate(_spawnObject, spawnPos, Quaternion.identity);
            var spawnedCollider = spawnedObject.GetComponent<Collider>();

            if (spawnedCollider != null && ignoreColliders?.Length > 0)
            {
                foreach (var ignoreCollider in ignoreColliders)
                {
                    Physics.IgnoreCollision(spawnedCollider, ignoreCollider);
                }
            }
            
            spawnedObject.AddForce(forceDirection * _spawnForce, _forceMode);
        }
    }
}