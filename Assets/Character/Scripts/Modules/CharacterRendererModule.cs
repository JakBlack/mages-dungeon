﻿using UnityEngine;
using UnityEngine.Rendering;

namespace MagesDungeon.Character.Modules
{
    public class CharacterRendererModule : CharacterModuleBase
    {
        [SerializeField] private Renderer[] _renderers;

        public override CharacterModuleInitializationArgs InitializeModule(CharacterModuleInitializationArgs args)
        {
            if (args.IsOwner)
            {
                foreach (var r in _renderers)
                {
                    r.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
                }
            }

            return base.InitializeModule(args);
        }
    }
}