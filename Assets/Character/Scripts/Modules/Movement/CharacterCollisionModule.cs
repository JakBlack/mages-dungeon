﻿using MagesDungeon.Extensions;
using MagesDungeon.PhysicsBehaviour;
using UnityEditor;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterCollisionModule : CharacterModuleBase
    {
        [SerializeField] private LayerMask _groundSnapLayerMask;
        [SerializeField] private float _groundSnapMaxDistance = 1f;
        [SerializeField] private CollisionCounter _collisionCounter;
        [SerializeField] private Rigidbody _parentBody;
        [SerializeField] private float _maxSnapSpeed = 15f;

        private RaycastHit _hit;
        private Vector3 _raycastOrigin;
        private bool _wasSnapSuccessful;
        private bool _wasTouchingGround;
        private Vector3 _connectedBodyVelocity;

        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            var groundNormal = _collisionCounter.GroundNormal;
            var slopeNormal = _collisionCounter.SlopeNormal;
            var isTouchingSlope = slopeNormal != default;
            var isTouchingGround = groundNormal != default;
            _connectedBodyVelocity = _collisionCounter.ConnectedBodyVelocity;
            
            if (isTouchingSlope && !isTouchingGround && _collisionCounter.IsCollisionNormalValid(slopeNormal))
            {
                groundNormal = slopeNormal;
                isTouchingGround = true;
            }

            args.DidSnapToGround = !isTouchingGround && TrySnapToGround(_wasTouchingGround, args.MovementDataArgs, ref groundNormal);
            args.IsTouchingGroundOrDidSnap = isTouchingGround || args.DidSnapToGround;
            args.IsTouchingSlope = isTouchingSlope;
            args.ConnectedBodyVelocity = _connectedBodyVelocity;
            args.TouchNormal = args.IsTouchingGroundOrDidSnap
                ? groundNormal
                : isTouchingSlope
                    ? slopeNormal
                    : transform.up;

            _wasTouchingGround = isTouchingGround;

            return base.FixedUpdateModule(args);
        }
        
        private bool TrySnapToGround(bool wasTouchingGround, CharacterMovementDataArgs movementDataArgs, ref Vector3 touchNormal)
        {
            var willSnap = wasTouchingGround
                           && movementDataArgs.CurrentVelocity.magnitude < _maxSnapSpeed
                           && TryGetLeftGroundNormal(out touchNormal);

            return willSnap;
        }

        private bool TryGetLeftGroundNormal(out Vector3 leftGroundNormal)
        {
            _raycastOrigin = transform.position;
            _wasSnapSuccessful = false;
            if (Physics.Raycast(_raycastOrigin, Vector3.down, out _hit, _groundSnapMaxDistance, _groundSnapLayerMask))
            {
                if (_collisionCounter.IsCollisionNormalValid(_hit.normal))
                {
                    leftGroundNormal = _hit.normal;
                    _wasSnapSuccessful = true;
                    _connectedBodyVelocity = _hit.rigidbody.GetConnectedBodyVelocity(_parentBody);

                    return true;
                }
            }

            leftGroundNormal = default;

            return false;
        }

        #region Gizmos

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                if (_hit.collider != null)
                {
                    Handles.color = _wasSnapSuccessful ? Color.cyan : Color.magenta;
                    Handles.DrawWireCube(_hit.point, Vector3.one * 0.2f);
                    Handles.DrawLine(_hit.point, _hit.point + _hit.normal, 2f);
                    Handles.DrawWireCube(_raycastOrigin, Vector3.one * 0.2f);
                }
            }
        }
#endif

        #endregion
    }
}