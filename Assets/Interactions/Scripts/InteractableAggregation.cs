﻿using System.Linq;
using UnityEngine;

namespace MagesDungeon.Interactions
{
    public class InteractableAggregation : InteractableHighlightedBase
    {
        [SerializeField] private Interactable[] _interactables;

        public override bool IsInteractable
            => _interactables.All(interactable => interactable.IsInteractable);

        protected override void DoInteract()
        {
            foreach (var interactable in _interactables)
            {
                interactable.Interact();
            }
        }
    }
}