﻿using UnityEngine;

namespace MagesDungeon
{
    public class SingletonMonoBehaviour<T> : MonoBehaviour
        where T : MonoBehaviour
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this as T;
                DontDestroyOnLoad(this);
            }
            else
            {
                Debug.LogError($"Instance of {typeof(T)} already exists, duplicate will be deleted");
                Destroy(this);
            }
        }
    }
}