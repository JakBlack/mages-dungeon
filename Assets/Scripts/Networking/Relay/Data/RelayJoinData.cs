﻿using Unity.Netcode.Transports.UTP;
using Unity.Services.Relay.Models;

namespace MagesDungeon.Networking.Relay
{
    public class RelayJoinData : RelayConnectionDataBase
    {
        private readonly byte[] _hostConnectionData;
        
        public RelayJoinData(JoinAllocation allocation, string joinCode)
            : base(allocation.ServerEndpoints, allocation.AllocationIdBytes,
                allocation.ConnectionData, allocation.Key, joinCode)
        {
            _hostConnectionData = allocation.HostConnectionData;
        }

        public override void ApplyToTransport(UnityTransport transport)
        {
            transport.SetClientRelayData(Ipv4Address, Port, AllocationIdBytes, Key, ConnectionData, _hostConnectionData,
                true);
        }
    }
}