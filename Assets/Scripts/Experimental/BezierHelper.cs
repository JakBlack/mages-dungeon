﻿using System.Collections.Generic;
using UnityEngine;

namespace MagesDungeon.Experimental
{
    public static class BezierHelper
    {
        public static Vector3[] GetInitialVectorArray(int interpolationPoints)
        {
            interpolationPoints = Mathf.Max(0, interpolationPoints);
            
            return new Vector3[interpolationPoints + 2];
        }
        
        public static void CalculateBezier(Vector3[] referencePoints, ref Vector3[] bezierPoints)
        {
            var referenceCount = referencePoints.Length;
            var bezierPointsCount = bezierPoints.Length;
            var lastPointIndex = bezierPointsCount - 1;
            var bufferPoints = new Vector3[referenceCount];

            bezierPoints[0] = referencePoints[0];
            bezierPoints[lastPointIndex] = referencePoints[referenceCount - 1];

            for (var i = 1; i < bezierPointsCount - 1; i++)
            {
                var value = (float) i / bezierPointsCount;

                for (var j = 0; j < referenceCount; j++)
                {
                    bufferPoints[j] = referencePoints[j];
                }
                
                bezierPoints[i] = GetBezierPoint(value, bufferPoints);
            }
        }
        
        /// <remarks>Modifies <see cref="referencePoints"/> in order to decrease allocations,
        /// keep this in mind, if you want to calculate multiple points with one array of reference points</remarks>
        private static Vector3 GetBezierPoint(float value, IList<Vector3> referencePoints)
        {
            var pointsCount = referencePoints.Count;

            while (pointsCount > 2)
            {
                pointsCount--;
                var lastPointIndex = pointsCount;

                for (var i = 0; i <= pointsCount; i++)
                {
                    if (i == lastPointIndex)
                    {
                        referencePoints[i] = referencePoints[pointsCount];
                    }
                    else
                    {
                        var currentPoint = referencePoints[i];
                        var nextPoint = referencePoints[i + 1];

                        referencePoints[i] = Vector3.Lerp(currentPoint, nextPoint, value);
                    }
                }
            }
            
            return Vector3.Lerp(referencePoints[0], referencePoints[1], value);
        }
    }
}