﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterJumpModule : CharacterModuleBase
    {
        [SerializeField] private float _jumpHeight = 2.0f;
        [SerializeField] private ForceMode _jumpForceMode;
        [SerializeField] private int _jumpCountCap = 2;
        [SerializeField] private int _maxStepsAsJumpedState = 2;
        
        private int _jumpCount;
        private int _stepsSinceLastJump;
        
        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            var canJump = _jumpCount < _jumpCountCap;
            
            if (canJump && args.InputData.IsJumpClicked.TryGetValidInput(out _))
            {
                DoJump(args);
                _stepsSinceLastJump = 0;
            }
            else
            {
                _stepsSinceLastJump++;
            }

            args.DidJumpRecently = _stepsSinceLastJump <= _maxStepsAsJumpedState;
            ResetJumpCounter(args);
            return base.FixedUpdateModule(args);
        }

        private void ResetJumpCounter(CharacterModuleFixedUpdateArgs args)
        {
            if (args.IsTouchingGroundOrDidSnap && !args.DidJumpRecently)
            {
                _jumpCount = 0;
            }
        }

        private void DoJump(CharacterModuleFixedUpdateArgs args)
        {
            var jumpDirection = args.TouchNormal;
            var moveArgs = args.MovementDataArgs;
            var jumpSpeed = Mathf.Sqrt(-2 * Physics.gravity.y * _jumpHeight);
            jumpSpeed = Mathf.Max(jumpSpeed - moveArgs.CurrentVelocity.y, 0);
            jumpDirection = (Vector3.up + jumpDirection).normalized;
            var velocityDelta = jumpDirection * jumpSpeed;

            moveArgs.MovementBody.AddForce(velocityDelta, _jumpForceMode);
            _jumpCount++;
        }
    }
}