﻿using MagesDungeon.Input;
using UnityEngine;

namespace MagesDungeon.Character.Input
{
    public class CharacterInputData
    {
        public Vector2 MovementDirection;
        public IBufferedInput<Vector2> LookDirection;
        public IBufferedInput<bool> IsFireClicked;
        public IBufferedInput<bool> IsAltFireClicked;
        public IBufferedInput<bool> IsDashClicked;
        public IBufferedInput<bool> IsJumpClicked;
        public IBufferedInput<bool> IsInteractClicked;
    }
}