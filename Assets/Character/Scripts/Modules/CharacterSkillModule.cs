﻿using MagesDungeon.Skills;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterSkillModule : CharacterModuleBase
    {
        [SerializeField] private SkillBase _mainSkill;
        [SerializeField] private SkillBase _altSkill;
        [SerializeField] private Transform _targetingTransform;
        [SerializeField] private Transform _spawnReferenceTransform;
        [SerializeField] private Collider[] _ignoreColliders;
        [SerializeField] private SkillCooldown _primaryCooldown;
        [SerializeField] private SkillCooldown _secondaryCooldown;
        
        public override CharacterModuleUpdateArgs UpdateModule(CharacterModuleUpdateArgs args)
        {
            if (args.InputData.IsFireClicked.TryGetValidInput(out var isClicked) && isClicked)
            {
                TryUseSkill(_mainSkill, _primaryCooldown);
            }
            if (args.InputData.IsAltFireClicked.TryGetValidInput(out var isAltClicked) && isAltClicked)
            {
                TryUseSkill(_altSkill, _secondaryCooldown);
            }
            
            return base.UpdateModule(args);
        }

        private void TryUseSkill(SkillBase skill, SkillCooldown skillCooldown)
        {
            var isSkillUsed = skill != null
                              && skillCooldown.IsStackAvailable
                              && skill.UseSkill(GetSkillUseArgs()).IsSuccessful;

            if (isSkillUsed)
            {
                skillCooldown.TryUseStack();
            }
        }

        private SkillUseArgs GetSkillUseArgs()
        {
            return new SkillUseArgs
            {
                TargetingTransform = _targetingTransform,
                SpawnReferenceTransform = _spawnReferenceTransform,
                IgnoreColliders = _ignoreColliders,
            };
        }
    }
}