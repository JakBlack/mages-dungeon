﻿using UnityEngine;

namespace MagesDungeon.Explosions
{
    public static class ExplosionUtils
    {
        private static readonly Collider[] Collisions = new Collider[256];

        public static void ApplyExplosion(Vector3 origin, ExplosionSettings explosionSettings)
        {
            var overlapsCount = Physics.OverlapSphereNonAlloc(origin, explosionSettings.ExplosionRadius, Collisions);
                
            for (var i = 0; i < overlapsCount; i++)
            {
                var coll = Collisions[i];
                if (coll == null)
                {
                    break;
                }

                var rb = coll.attachedRigidbody;
                explosionSettings.ApplyExplosion(rb, origin);
            }
        }
    }
}