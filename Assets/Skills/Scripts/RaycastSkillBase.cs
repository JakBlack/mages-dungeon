﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    [CreateAssetMenu(menuName = "Player/Skills/Raycast/Pure Raycast", fileName = nameof(RaycastSkillBase))]
    public class RaycastSkillBase : SkillBase
    {
        [SerializeField] private float _maxDistance = 100f;
        [SerializeField] private LayerMask _raycastMask;
        [SerializeField] private bool _forceShoot;
        
        public override SkillUseResultArgs UseSkill(SkillUseArgs args)
        {
            var didHit = TryShoot(args.TargetingTransform, out var hit, _forceShoot);

            return new SkillUseResultArgs
            {
                IsSuccessful = didHit,
                TargetHit = hit,
            };
        }
        
        protected bool TryShoot(Transform targetingTransform, out RaycastHit raycastHit, bool forceShoot = false)
        {
            var rayOrigin = targetingTransform.position;
            var rayDirection = targetingTransform.forward;
            var didHit = Physics.Raycast(rayOrigin, rayDirection,
                out var hit, _maxDistance, _raycastMask);

            raycastHit = hit;

            if (forceShoot && !didHit)
            {
                raycastHit.point = rayOrigin + rayDirection * _maxDistance;
                didHit = true;
            }

            return didHit;
        }
    }
}