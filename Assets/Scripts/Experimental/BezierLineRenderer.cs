﻿using UnityEngine;

namespace MagesDungeon.Experimental
{
    [ExecuteAlways]
    public class BezierLineRenderer : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer _lineRenderer = null;
        [SerializeField, Tooltip("Should contain at least 2 items")]
        private Transform[] _referenceTransforms = null;
        [SerializeField, Min(0)]
        private int _interpolationPoints = 2;

        private bool _canRender;
        private Vector3[] _bezierPoints;
        private Vector3[] _referencePoints;

        private void Start()
        {
            Initialize();
        }

        private void OnValidate()
        {
            Initialize();
        }

        private void Update()
        {
            if (_canRender)
            {
                RecalculatePoints();
                _lineRenderer.SetPositions(_bezierPoints);
            }
        }

        private void RecalculatePoints()
        {
            for (int i = 0, l = _referencePoints.Length; i < l; i++)
            {
                _referencePoints[i] = _referenceTransforms[i].localPosition;
            }
            
            BezierHelper.CalculateBezier(_referencePoints, ref _bezierPoints);
        }

        private void Initialize()
        {
            _canRender = _lineRenderer != null && _referenceTransforms.Length >= 2;

            if (!_canRender)
            {
                Debug.LogWarning("Can't render bezier curve!", this);
            }
            else
            {
                var pointsCount = 2 + _interpolationPoints;
            
                _bezierPoints = new Vector3[pointsCount];
                _lineRenderer.positionCount = pointsCount;
                _referencePoints = new Vector3[_referenceTransforms.Length];
            }
        }
    }
}