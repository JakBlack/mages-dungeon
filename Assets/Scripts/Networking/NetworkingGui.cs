﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UniRx;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

namespace MagesDungeon.Networking
{
    public class NetworkingGui : NetworkBehaviour
    {
        [Serializable]
        private class ButtonStateTuple
        {
            public NetworkingState[] SupportedStates;
            public GameObject TargetObject;
        }

        [SerializeField] private ButtonStateTuple[] _stateTuples;
        [SerializeField] private TMP_InputField _joinCodeField;
        [Header("Buttons")]
        [SerializeField] private Button _hostButton;
        [SerializeField] private Button _connectButton;
        [SerializeField] private Button _disconnectButton;
        
        private readonly List<IDisposable> _disposables = new();
        
        private void Start()
        {
            var subscription = NetworkingFacade.Instance.State.AsObservable().Subscribe(UpdateStateObjects);
            _disposables.Add(subscription);
            _hostButton.onClick.AddListener(OnHostClick);
            _connectButton.onClick.AddListener(OnConnectClick);
            _disconnectButton.onClick.AddListener(OnDisconnectClick);
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
        }

        private void UpdateStateObjects(NetworkingState state)
        {
            foreach (var stateTuple in _stateTuples)
            {
                stateTuple.TargetObject.SetActive(stateTuple.SupportedStates.Contains(state));
            }
        }

        private void OnDisconnectClick()
        {
            NetworkingFacade.Instance.Shutdown();
        }

        private void OnConnectClick()
        {
            NetworkingFacade.Instance.StartClient(_joinCodeField.text);
        }

        private void OnHostClick()
        {
            NetworkingFacade.Instance.StartHost();
        }
    }
}