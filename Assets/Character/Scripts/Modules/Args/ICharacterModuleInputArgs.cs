﻿using MagesDungeon.Character.Input;

namespace MagesDungeon.Character.Modules
{
    public interface ICharacterModuleInputArgs
    {
        CharacterInputData InputData { get; set; }
    }
}