﻿using MagesDungeon.Character.Input;

namespace MagesDungeon.Character.Modules
{
    public struct CharacterModuleLateUpdateArgs : ICharacterModuleInputArgs
    {
        public CharacterInputData InputData { get; set; }
    }
}