﻿namespace MagesDungeon.Input
{
    public interface IBufferedInput<T> where T : struct
    {
        bool IsInputValid();
        T GetInputValue(bool markAsUsed = true);
        bool TryGetValidInput(out T input, bool markAsUsed = true);
    }
}