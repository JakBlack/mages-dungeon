﻿using UnityEditor;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterMovementDataModule : CharacterModuleBase
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private float _walkSpeed = 2.0f;
        [SerializeField] private float _acceleration = 10.0f;
        
        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            args.MovementDataArgs.Acceleration = _acceleration;
            args.MovementDataArgs.Speed = _walkSpeed;
            args.MovementDataArgs.CurrentVelocity = _rigidbody.velocity;
            args.MovementDataArgs.MovementBody = _rigidbody;
            
            return base.FixedUpdateModule(args);
        }
        
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                var center = _rigidbody.position;
                Handles.color = Color.yellow;
                Handles.DrawLine(center, center + _rigidbody.velocity * 0.1f, 2f);
            }
        }
#endif
    }
}