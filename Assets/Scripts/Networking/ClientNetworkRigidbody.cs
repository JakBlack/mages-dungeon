﻿using Unity.Netcode;
using UnityEngine;

namespace MagesDungeon.Networking
{
    [RequireComponent(typeof(Rigidbody), typeof(ClientNetworkTransform))]
    public class ClientNetworkRigidbody : NetworkBehaviour
    {
        private Rigidbody _rigidbody;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void AddExplosionForce(
            float explosionForce,
            Vector3 explosionPosition,
            float explosionRadius,
            float upwardsModifier = 0.0f,
            ForceMode mode = ForceMode.Force)
        {
            AddExplosionForceClientRpc(explosionForce, explosionPosition, explosionRadius, upwardsModifier, mode);
        }

        [ClientRpc]
        private void AddExplosionForceClientRpc(
            float explosionForce,
            Vector3 explosionPosition,
            float explosionRadius,
            float upwardsModifier = 0.0f,
            ForceMode mode = ForceMode.Force)
        {
            _rigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, upwardsModifier, mode);
        }
    }
}