﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    [CreateAssetMenu(menuName = "Player/Skills/Raycast/Obstacles Spawn", fileName = nameof(RaycastSpawnSkill))]
    public class RaycastSpawnSkill : RaycastSkillBase
    {
        [SerializeField] private GameObject _obstaclePrefab;
        [SerializeField] private int _itemsPerFrame = 10;
        
        public override SkillUseResultArgs UseSkill(SkillUseArgs args)
        {
            var result = base.UseSkill(args);
            if (result.IsSuccessful)
            {
                SpawnObstacles(result.TargetHit);
            }
            
            return result;
        }

        private void SpawnObstacles(RaycastHit hit)
        {
            var spawnPos = hit.point;
            var spawnRot = Quaternion.identity;

            for (var i = 0; i < _itemsPerFrame; i++)
            {
                Instantiate(_obstaclePrefab, spawnPos, spawnRot);
            }
        }
    }
}