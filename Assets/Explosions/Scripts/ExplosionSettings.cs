﻿using UnityEngine;

namespace MagesDungeon.Explosions
{
    [CreateAssetMenu(menuName = "Explosions/ExplosionSettings", fileName = nameof(ExplosionSettings))]
    public class ExplosionSettings : ScriptableObject
    {
        [SerializeField] private ExplosionEffect _effectPrefab;
        [SerializeField] private ForceMode _forceMode;
        [SerializeField] private float _explosionRadius = 5.0f;
        [SerializeField] private float _explosionForce = 1000;
        [SerializeField] private Vector3 _directionModifier = new(0, 1, 0);
        [SerializeField] private AnimationCurve _forceCurve = AnimationCurve.Linear(0, 1, 1, 1);

        public float ExplosionRadius => _explosionRadius;

        public void ApplyExplosion(Rigidbody targetRigidbody, Vector3 forceOrigin)
        {
            if (targetRigidbody != null)
            {
                var targetPosition = targetRigidbody.position;
                var forceDirection = targetPosition - forceOrigin;
                var distance = forceDirection.magnitude;
                var explosionForce = (forceDirection.normalized.normalized + _directionModifier).normalized
                                     * _explosionForce * _forceCurve.Evaluate(distance / _explosionRadius);
                
                targetRigidbody.AddForce(explosionForce, _forceMode);
            }
        }

        public void SpawnExplosion(Vector3 position)
        {
            var explosionInstance = Instantiate(_effectPrefab, position, Quaternion.identity);
            explosionInstance.Initialize(this);
            ExplosionUtils.ApplyExplosion(position, this);
        }
    }
}