﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    public struct SkillUseArgs
    {
        public Transform TargetingTransform;
        public Transform SpawnReferenceTransform;
        public Collider[] IgnoreColliders;
    }
}