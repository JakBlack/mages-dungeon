﻿namespace MagesDungeon.Input
{
    internal interface IUpdatableBufferedInput
    {
        void UpdateBufferTimer(float deltaTime);
    }
}