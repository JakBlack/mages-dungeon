﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterPhysicMaterialModule : CharacterModuleBase
    {
        [SerializeField] private Collider[] _colliders;

        private PhysicMaterial _physicMaterial;
        private bool _isFrictionEnabled;
        
        public override CharacterModuleInitializationArgs InitializeModule(CharacterModuleInitializationArgs args)
        {
            _physicMaterial = new PhysicMaterial();
            foreach (var coll in _colliders)
            {
                coll.sharedMaterial = _physicMaterial;
            }
            UpdatePhysicsMaterial(true);

            return base.InitializeModule(args);
        }

        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            var shouldEnableFriction = !args.IsInputValid && args.IsTouchingGroundOrDidSnap;
            UpdatePhysicsMaterial(shouldEnableFriction);
            
            return base.FixedUpdateModule(args);
        }

        private void OnDestroy()
        {
            if (_physicMaterial != null)
            {
                Destroy(_physicMaterial);
            }
        }

        private void UpdatePhysicsMaterial(bool shouldEnableFriction, bool forceUpdate = false)
        {
            if (shouldEnableFriction != _isFrictionEnabled || forceUpdate)
            {
                var friction = shouldEnableFriction ? 1 : 0;
                var frictionCombineMode = shouldEnableFriction ? PhysicMaterialCombine.Maximum : PhysicMaterialCombine.Minimum;
                
                _physicMaterial.dynamicFriction = friction;
                _physicMaterial.staticFriction = friction;
                _physicMaterial.frictionCombine = frictionCombineMode;
                
                _isFrictionEnabled = shouldEnableFriction;
            }
        }
    }
}