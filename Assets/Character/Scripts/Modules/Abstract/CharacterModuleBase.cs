﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public abstract class CharacterModuleBase : MonoBehaviour
    {
        public virtual CharacterModuleInitializationArgs InitializeModule(CharacterModuleInitializationArgs args)
        {
            return args;
        }

        public virtual CharacterModuleUpdateArgs UpdateModule(CharacterModuleUpdateArgs args)
        {
            return args;
        }

        public virtual CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            return args;
        }

        public virtual CharacterModuleLateUpdateArgs LateUpdateModule(CharacterModuleLateUpdateArgs args)
        {
            return args;
        }
    }
}