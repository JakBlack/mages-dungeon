﻿namespace MagesDungeon.Networking
{
    public enum NetworkingState
    {
        None = 0,
        ChangingState = 1,
        Connected = 2,
    }
}