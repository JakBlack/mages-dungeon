﻿using MagesDungeon.Character.Input;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public struct CharacterModuleFixedUpdateArgs : ICharacterModuleInputArgs
    {
        public CharacterInputData InputData { get; set; }
        public CharacterMovementDataArgs MovementDataArgs;
        public bool IsTouchingGroundOrDidSnap;
        public bool IsTouchingSlope;
        public bool DidSnapToGround;
        public Vector3 TouchNormal;
        public Vector3 ConnectedBodyVelocity;
        public bool IsInputValid;
        public bool DidJumpRecently;
    }
}