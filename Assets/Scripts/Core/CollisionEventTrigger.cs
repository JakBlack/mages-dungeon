﻿using UnityEngine;
using UnityEngine.Events;

namespace MagesDungeon
{
    public class CollisionEventTrigger : MonoBehaviour
    {
        [SerializeField] private UnityEvent<Collision> CollisionEnter;
        [SerializeField] private UnityEvent<Collision> CollisionExit;
        [SerializeField] private UnityEvent<Collision> CollisionStay;

        private void OnCollisionEnter(Collision other)
        {
            CollisionEnter.Invoke(other);
        }

        private void OnCollisionExit(Collision other)
        {
            CollisionExit.Invoke(other);
        }

        private void OnCollisionStay(Collision other)
        {
            CollisionStay.Invoke(other);
        }
    }
}