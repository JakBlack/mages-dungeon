﻿using UnityEngine;

namespace MagesDungeon.Explosions
{
    public class ExplosionBehaviour : MonoBehaviour
    {
        [SerializeField] private ExplosionSettings _explosionSettings;
        [SerializeField, Min(0)] private int _explosionsUntilDestruction = 1;

        public void ApplyExplosion(Vector3 position)
        {
            _explosionSettings.SpawnExplosion(position);
            _explosionsUntilDestruction--;
            if (_explosionsUntilDestruction <= 0)
            {
                Destroy(gameObject);
            }
        }

        public void ApplyExplosionAtCurrentPosition()
        {
            ApplyExplosion(transform.position);
        }
    }
}