﻿using MagesDungeon.Character.Modules;
using MagesDungeon.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MagesDungeon.Character.Input
{
    public class CharacterInputModule : CharacterInputModuleBase
    {
        [SerializeField] private BufferedInputVector2 _movementDirection;
        [SerializeField] private BufferedInputVector2 _lookDirection;
        [SerializeField] private BufferedInputBool _fireClicked;
        [SerializeField] private BufferedInputBool _altFireClicked;
        [SerializeField] private BufferedInputBool _dashClicked;
        [SerializeField] private BufferedInputBool _jumpClicked;
        [SerializeField] private BufferedInputBool _interactClicked;

        private IUpdatableBufferedInput[] _updatableBufferedInputs;
        private CharacterInputData _inputData;

        public override void Initialize()
        {
            _updatableBufferedInputs = new IUpdatableBufferedInput[]
            {
                _movementDirection,
                _lookDirection,
                _fireClicked,
                _altFireClicked,
                _dashClicked,
                _jumpClicked,
            };
            _inputData = new CharacterInputData
            {
                LookDirection = _lookDirection,
                IsFireClicked = _fireClicked,
                IsAltFireClicked = _altFireClicked,
                IsDashClicked = _dashClicked,
                IsJumpClicked = _jumpClicked,
                IsInteractClicked = _interactClicked,
            };
        }

        public override CharacterInputData GetInputData()
        {
            _inputData.MovementDirection = _movementDirection.GetInputValue();
            
            return _inputData;
        }

        private void Update()
        {
            var dt = Time.deltaTime;
            foreach (var input in _updatableBufferedInputs)
            {
                input.UpdateBufferTimer(dt);
            }
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            _movementDirection.SetInputValue(context.ReadValue<Vector2>());
        }

        public void OnLook(InputAction.CallbackContext context)
        {
            _lookDirection.SetInputValue(context.ReadValue<Vector2>());
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            _fireClicked.SetInputValue(context.action.triggered);
        }

        public void OnDash(InputAction.CallbackContext context)
        {
            _dashClicked.SetInputValue(context.action.triggered);
        }

        public void OnAltFire(InputAction.CallbackContext context)
        {
            _altFireClicked.SetInputValue(context.action.triggered);
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            _jumpClicked.SetInputValue(context.action.triggered);
        }

        public void OnInteract(InputAction.CallbackContext context)
        {
            _interactClicked.SetInputValue(context.action.triggered);
        }
    }
}