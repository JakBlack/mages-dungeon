﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterCameraModule : CharacterModuleBase
    {
        private const float HalfRotationAngle = 180.0f;
        private const float FullRotationAngle = 360.0f;

        [SerializeField] private Camera _camera;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private Transform _cameraTransform;
        [SerializeField] private float _minXAngle = -90.0f;
        [SerializeField] private float _maxXAngle = 50.0f;

        public override CharacterModuleInitializationArgs InitializeModule(CharacterModuleInitializationArgs args)
        {
            _camera.enabled = args.IsOwner;

            return base.InitializeModule(args);
        }

        public override CharacterModuleLateUpdateArgs LateUpdateModule(CharacterModuleLateUpdateArgs args)
        {
            var smoothDeltaTime = Time.smoothDeltaTime;
            var inputRotation = args.InputData.LookDirection.GetInputValue();
            var yaw = inputRotation.x;
            var pitch = inputRotation.y;
            
            if (!Mathf.Approximately(yaw, 0))
            {
                _cameraTransform.Rotate(_playerTransform.up, yaw * smoothDeltaTime, Space.World);
            }
            
            if (!Mathf.Approximately(pitch, 0))
            {
                var rotation = _cameraTransform.localEulerAngles.x;
                if (rotation >= HalfRotationAngle)
                {
                    rotation -= FullRotationAngle;
                }

                var rotationDelta = pitch * smoothDeltaTime;
                rotationDelta = Mathf.Clamp(rotation + rotationDelta, _minXAngle, _maxXAngle) - rotation;

                _cameraTransform.Rotate(Vector3.right, rotationDelta);
            }
            
            return base.LateUpdateModule(args);
        }
    }
}