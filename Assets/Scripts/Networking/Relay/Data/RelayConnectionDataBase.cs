﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode.Transports.UTP;
using Unity.Services.Relay.Models;

namespace MagesDungeon.Networking.Relay
{
    public abstract class RelayConnectionDataBase
    {
        private const string DtlsConnectionType = "dtls";

        protected readonly string Ipv4Address;
        protected readonly ushort Port;
        protected readonly byte[] AllocationIdBytes;
        protected readonly byte[] ConnectionData;
        protected readonly byte[] Key;
        
        public string JoinCode { get; }
        
        public bool IsValid => !string.IsNullOrEmpty(Ipv4Address);

        protected RelayConnectionDataBase(IEnumerable<RelayServerEndpoint> endpoints,
            byte[] allocationIdBytes, byte[] connectionData, byte[] key, string joinCode)
        {
            var dtlsEndpoint = endpoints.First(e =>
                e.ConnectionType.Equals(DtlsConnectionType, StringComparison.InvariantCultureIgnoreCase));
            
            Ipv4Address = dtlsEndpoint.Host;
            Port = (ushort)dtlsEndpoint.Port;
            
            AllocationIdBytes = allocationIdBytes;
            ConnectionData = connectionData;
            Key = key;
            JoinCode = joinCode;
        }

        public abstract void ApplyToTransport(UnityTransport transport);
    }
}