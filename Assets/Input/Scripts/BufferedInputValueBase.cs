﻿using System;
using UnityEngine;

namespace MagesDungeon.Input
{
    [Serializable]
    public abstract class BufferedInputBase<T> : IUpdatableBufferedInput, IBufferedInput<T>
        where T : struct
    {
        private T _inputValue;
        private float _inputTimePassed;
        private bool _wasInputUsed;
        
        [SerializeField] private bool _useInputBuffer;
        [SerializeField, Range(0, 1)] private float _bufferDuration = .2f;
        
        private bool IsTimeBufferValid => !_useInputBuffer || _inputTimePassed < _bufferDuration;

        public void SetInputValue(T value)
        {
            if (!_useInputBuffer || IsInputValidInternal(value))
            {
                _inputValue = value;
                _inputTimePassed = 0;
                _wasInputUsed = false;
            }
        }

        public void UpdateBufferTimer(float deltaTime)
        {
            if (_useInputBuffer && _inputTimePassed < _bufferDuration)
            {
                _inputTimePassed += deltaTime;
            }
        }

        public bool IsInputValid()
        {
            return !_wasInputUsed && IsInputValidInternal(_inputValue) && IsTimeBufferValid;
        }

        public T GetInputValue(bool markAsUsed = true)
        {
            if (markAsUsed)
            {
                _wasInputUsed = true;
            }

            return _inputValue;
        }

        public bool TryGetValidInput(out T input, bool markAsUsed = true)
        {
            var isInputValid = IsInputValid();
            input = GetInputValue(markAsUsed && isInputValid);

            return isInputValid;
        }

        protected abstract bool IsInputValidInternal(T input);
    }
}