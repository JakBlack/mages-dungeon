﻿using System;
using UnityEngine;

namespace MagesDungeon.Input
{
    [Serializable]
    public class BufferedInputVector2 : BufferedInputBase<Vector2>
    {
        protected override bool IsInputValidInternal(Vector2 input)
        {
            return input != default;
        }
    }
}