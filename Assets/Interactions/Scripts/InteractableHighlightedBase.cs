﻿using MagesDungeon.Effects;
using UnityEngine;

namespace MagesDungeon.Interactions
{
    public abstract class InteractableHighlightedBase : Interactable
    {
        [Header("Effect")]
        [SerializeField] private HighlightEffect _highlightEffect;
        [SerializeField] private Color _interactableColor = Color.green;
        [SerializeField] private Color _nonInteractableColor = Color.red;
        
        private bool _isInteractable;

        [ContextMenu(nameof(Interact))]
        public override void Interact()
        {
            if (IsInteractable)
            {
                DoInteract();
            }
        }

        protected abstract void DoInteract();

        public override void SetHighlightState(bool isHighlightActive)
        {
            _highlightEffect.SetHighlightBlend(isHighlightActive ? 1 : 0);
        }

        private void LateUpdate()
        {
            var isInteractable = IsInteractable;
            if (isInteractable != _isInteractable)
            {
                _isInteractable = isInteractable;
                UpdateHighlightColor(isInteractable);
            }
        }

        private void UpdateHighlightColor(bool isInteractable)
        {
            var color = isInteractable ? _interactableColor : _nonInteractableColor;
            
            _highlightEffect.SetHighlightColor(color);
        }
    }
}