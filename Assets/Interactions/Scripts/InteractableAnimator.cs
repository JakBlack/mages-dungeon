﻿using UnityEngine;

namespace MagesDungeon.Interactions
{
    public class InteractableAnimator : InteractableHighlightedBase
    {
        [Header("Animator")]
        [SerializeField] private Animator _animator;
        [SerializeField] private string _triggerName = "ChangeState";

        public override bool IsInteractable
        {
            get
            {
                var animatorState = _animator.GetCurrentAnimatorStateInfo(0);
                
                return animatorState.loop || animatorState.normalizedTime is <= 0 or >= 1;
            }
        }
        
        protected override void DoInteract()
        {
            _animator.SetTrigger(_triggerName);
        }
    }
}