﻿using MagesDungeon.Interactions;
using MagesDungeon.Skills;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterInteractionModule : CharacterModuleBase
    {
        [SerializeField] private Transform _targetingTransform;
        [SerializeField] private SkillBase _interactionSkill;
        
        private bool _canInteract;
        private Interactable _interactable;
        
        public override CharacterModuleUpdateArgs UpdateModule(CharacterModuleUpdateArgs args)
        {
            var interactable = GetInteractable();
            var isNewInteractableNull = interactable == null;
            if (interactable != _interactable)
            {
                if (_interactable != null)
                {
                    _interactable.SetHighlightState(false);
                }
                if (!isNewInteractableNull)
                {
                    interactable.SetHighlightState(true);
                }
                _interactable = interactable;
            }

            if (!isNewInteractableNull
                && interactable.IsInteractable
                && args.InputData.IsInteractClicked.TryGetValidInput(out var isClicked)
                && isClicked)
            {
                interactable.Interact();
            }
            return base.UpdateModule(args);
        }

        private Interactable GetInteractable()
        {
            var args = new SkillUseArgs
            {
                TargetingTransform = _targetingTransform,
            };
            var skillResult = _interactionSkill.UseSkill(args);
            
            return skillResult.IsSuccessful
                ? skillResult.TargetHit.transform.GetComponent<Interactable>()
                : null;
        }
    }
}