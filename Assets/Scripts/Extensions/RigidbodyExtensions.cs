﻿using UnityEngine;

namespace MagesDungeon.Extensions
{
    public static class RigidbodyExtensions
    {
        public static Vector3 GetConnectedBodyVelocity(this Rigidbody rigidbody, Rigidbody agentBody)
        {
            return rigidbody.IsValidConnectedBody(agentBody.mass)
                ? rigidbody.GetPointVelocity(agentBody.position)
                : Vector3.zero;
        }

        public static bool IsValidConnectedBody(this Rigidbody rigidbody, float agentMass)
        {
            return rigidbody != null && (rigidbody.isKinematic || rigidbody.mass > agentMass);
        }
    }
}