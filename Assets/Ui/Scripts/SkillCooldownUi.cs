﻿using MagesDungeon.Skills;
using UnityEngine;

namespace MagesDungeon.Ui
{
    public class SkillCooldownUi : MonoBehaviour
    {
        [SerializeField] private SkillCooldown _skillCooldown;
        [SerializeField] private ProgressBar _progressBarPrefab;
        [SerializeField] private Transform _progressBarsParent;

        private ProgressBar[] _progressBars;

        private void Start()
        {
            _progressBars = new ProgressBar[_skillCooldown.MaxStacks];
            for (var i = 0; i < _progressBars.Length; i++)
            {
                _progressBars[i] = Instantiate(_progressBarPrefab, _progressBarsParent);
            }
        }

        private void Update()
        {
            for (int i = 0, l = _progressBars.Length; i < l; i++)
            {
                _progressBars[i].SetProgress(_skillCooldown.GetStackProgress(i));
            }
        }
    }
}