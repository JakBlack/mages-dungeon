﻿using UnityEngine;

namespace MagesDungeon.Explosions
{
    public class ExplosionEffect : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private ParticleSystem.MinMaxCurve _sizeCurve;

        private float _explosionRadius;

        public void Initialize(ExplosionSettings explosionSettings)
        {
            _explosionRadius = explosionSettings.ExplosionRadius;
            _sizeCurve.curveMultiplier = _explosionRadius * 2;
            var sizeSystem = _particleSystem.sizeOverLifetime;
            sizeSystem.size = _sizeCurve;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, _explosionRadius);
        }
#endif
    }
}