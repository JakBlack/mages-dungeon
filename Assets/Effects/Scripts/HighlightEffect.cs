﻿using UnityEngine;

namespace MagesDungeon.Effects
{
    public class HighlightEffect : MonoBehaviour
    {
        [SerializeField] private Renderer _renderer;
        [SerializeField] private string _highlightColorProperty = "HighlightColor";
        [SerializeField] private string _highlightBlendProperty = "HighlightBlend";

        public void SetHighlightColor(Color color)
        {
            _renderer.material.SetColor(_highlightColorProperty, color);
        }

        public void SetHighlightBlend(float blendValue)
        {
            _renderer.material.SetFloat(_highlightBlendProperty, blendValue);
        }
    }
}