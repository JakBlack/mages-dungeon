﻿using System.Collections;
using UnityEngine;

namespace MagesDungeon
{
    public class TimedDespawnBehaviour : MonoBehaviour
    {
        [SerializeField] private float _lifeDuration = 2.0f;
        
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(_lifeDuration);
            Destroy(gameObject);
        }
    }
}