﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    public class SkillCooldown : MonoBehaviour
    {
        [SerializeField] private int _maxStacks = 1;
        [SerializeField, Range(0, 120)] private float _stackCooldownTime = 3;
        [SerializeField] private bool _startFull = true;

        private int _availableStacks;
        private float _timeLeftToRegenerateStack;
        
        public bool IsStackAvailable => _availableStacks > 0;
        public bool IsMaxStacksReached => _availableStacks == _maxStacks;
        public float StackProgress => (_stackCooldownTime - _timeLeftToRegenerateStack) / _stackCooldownTime;
        public int MaxStacks => _maxStacks;

        public int AvailableStacks => _availableStacks;

        private void Awake()
        {
            if (_startFull)
            {
                _availableStacks = _maxStacks;
            }

            _timeLeftToRegenerateStack = _stackCooldownTime;
        }

        private void Update()
        {
            if (_availableStacks < _maxStacks)
            {
                if (_timeLeftToRegenerateStack <= 0)
                {
                    _availableStacks++;
                    _timeLeftToRegenerateStack = IsMaxStacksReached
                        ? _stackCooldownTime
                        : _stackCooldownTime + _timeLeftToRegenerateStack - Time.deltaTime;
                }
                else
                {
                    _timeLeftToRegenerateStack -= Time.deltaTime;
                }
            }
        }

        public bool TryUseStack()
        {
            var canUseStack = IsStackAvailable;
            if (canUseStack)
            {
                _availableStacks--;
            }

            return canUseStack;
        }

        public float GetStackProgress(int stackIndex)
        {
            if (stackIndex < _availableStacks)
            {
                return 1;
            }

            if (stackIndex > _availableStacks)
            {
                return 0;
            }

            return StackProgress;
        }
    }
}