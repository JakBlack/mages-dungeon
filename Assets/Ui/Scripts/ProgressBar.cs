﻿using UnityEngine;

namespace MagesDungeon.Ui
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField, Range(0, 1)] private float _progress;
        [SerializeField] private RectTransform _progressBar;

        private void OnValidate()
        {
            if (_progressBar != null)
            {
                UpdateVisualProgress();
            }
        }

        public void SetProgress(float progress)
        {
            _progress = progress;
            UpdateVisualProgress();
        }

        private void UpdateVisualProgress()
        {
            var anchorMax = _progressBar.anchorMax;
            anchorMax.x = _progress;

            _progressBar.anchorMax = anchorMax;
        }
    }
}