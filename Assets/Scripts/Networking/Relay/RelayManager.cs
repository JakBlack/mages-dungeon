﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UniRx;
using Unity.Netcode.Transports.UTP;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;

namespace MagesDungeon.Networking.Relay
{
    public class RelayManager : MonoBehaviour
    {
        [SerializeField] private string _envName = "production";
        [SerializeField] private UnityTransport _transport;
        [SerializeField] private int _maxPlayers = 4;
        
        private Task<List<Region>> _availableRegions;

        public ReactiveProperty<Region> SelectedRegion { get; } = new();
        public List<Region> AvailableRegions { get; private set; }

        private bool IsRelaySupported =>
            _transport != null && _transport.Protocol == UnityTransport.ProtocolType.RelayUnityTransport;

        public async Task FetchAvailableRegionsAsync(CancellationToken cancellationToken)
        {
            if (_availableRegions is not { IsCompletedSuccessfully: true, Result: { Count: not 0 } })
            {
                _availableRegions = GetAvailableRegionsInternalAsync(cancellationToken);
            }

            AvailableRegions = await _availableRegions;
        }

        public async Task<RelayHostData> HostAllocation(CancellationToken cancellationToken)
        {
            if (await TryApproveRelayAction(cancellationToken))
            {
                var allocation = await RelayService.Instance
                    .CreateAllocationAsync(_maxPlayers, SelectedRegion.Value?.Id);
                var joinCode = await RelayService.Instance
                    .GetJoinCodeAsync(allocation.AllocationId);

                return new RelayHostData(allocation, joinCode);
            }

            return default;
        }
        
        public async Task<RelayJoinData> JoinAllocation(string joinCode, CancellationToken cancellationToken)
        {
            if (await TryApproveRelayAction(cancellationToken))
            {
                var allocation = await RelayService.Instance
                    .JoinAllocationAsync(joinCode);

                return new RelayJoinData(allocation, joinCode);
            }

            return default;
        }

        private async Task<bool> TryApproveRelayAction(CancellationToken cancellationToken, bool checkSelectedRegion = true)
        {
            var isActionApproved = IsRelaySupported;

            if (isActionApproved)
            {
                if (UnityServices.State != ServicesInitializationState.Initialized)
                {
                    await UnityServices.InitializeAsync(new InitializationOptions().SetEnvironmentName(_envName));
                }
                if (!AuthenticationService.Instance.IsSignedIn)
                {
                    await AuthenticationService.Instance.SignInAnonymouslyAsync();
                    cancellationToken.ThrowIfCancellationRequested();
                }
            }

            isActionApproved &= !checkSelectedRegion || !string.IsNullOrEmpty(SelectedRegion.Value?.Id);

            return isActionApproved;
        }

        private async Task<List<Region>> GetAvailableRegionsInternalAsync(CancellationToken cancellationToken)
        {
            List<Region> availableRegions = default;
            if (await TryApproveRelayAction(cancellationToken, false))
            {
                availableRegions = await RelayService.Instance.ListRegionsAsync();
                cancellationToken.ThrowIfCancellationRequested();
            }

            return availableRegions;
        }
    }
}