﻿using MagesDungeon.Skills;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterDashModule : CharacterModuleBase
    {
        [SerializeField] private float _dashSpeed = 5;
        [SerializeField] private float _dashAcceleration = 300;
        [SerializeField] private float _dashDuration = 0.1f;
        [SerializeField] private AnimationCurve _dashSpeedCurve;
        [SerializeField] private SkillCooldown _skillCooldown;
        
        private float _remainingDashTime;
        private Vector2 _dashDirection;
        
        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            if (IsDashActive(args))
            {
                var t = (_dashDuration - _remainingDashTime) / _dashDuration;
                var evaluatedSpeed = _dashSpeed * _dashSpeedCurve.Evaluate(t);
                
                args.MovementDataArgs.Speed = Mathf.Max(evaluatedSpeed, args.MovementDataArgs.Speed);
                args.MovementDataArgs.Acceleration = _dashAcceleration;
                
                _remainingDashTime -= Time.fixedDeltaTime;
                if (!args.IsTouchingGroundOrDidSnap)
                {
                    args.MovementDataArgs.CurrentVelocity.y = 0;
                    args.MovementDataArgs.MovementBody.velocity = args.MovementDataArgs.CurrentVelocity;
                }
                args.InputData.MovementDirection = _dashDirection;
            }
            
            return base.FixedUpdateModule(args);
        }

        private bool IsDashActive(CharacterModuleFixedUpdateArgs args)
        {
            return _remainingDashTime > 0 || TryStartDash(args);
        }

        private bool TryStartDash(CharacterModuleFixedUpdateArgs args)
        {
            if (args.InputData.IsDashClicked.TryGetValidInput(out var isDashClicked)
                && isDashClicked
                && _skillCooldown.TryUseStack())
            {
                _remainingDashTime = _dashDuration;
                _dashDirection = args.InputData.MovementDirection;
                if (_dashDirection == default)
                {
                    _dashDirection = Vector2.up;
                }
                
                return true;
            }

            return false;
        }
    }
}