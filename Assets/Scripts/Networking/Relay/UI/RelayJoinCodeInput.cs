﻿using System;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;

namespace MagesDungeon.Networking.Relay
{
    public class RelayJoinCodeInput : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _joinCodeText;
        
        private readonly List<IDisposable> _disposables = new();

        private void Start()
        {
            var hostData = NetworkingFacade.Instance.RelayConnectionData;
            _disposables.Add(hostData.AsObservable().Subscribe(OnHostDataChanged));
            _joinCodeText.onValidateInput += OnValidateInput;
        }

        private void OnDestroy()
        {
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
        }

        private void OnHostDataChanged(RelayConnectionDataBase connectionData)
        {
            var joinCode = connectionData?.JoinCode;
            var isCodeValid = connectionData?.IsValid ?? false;
            _joinCodeText.SetTextWithoutNotify(joinCode);
            _joinCodeText.interactable = !isCodeValid;
        }
        
        private char OnValidateInput(string text, int charIndex, char addedChar)
        {
            return char.ToUpper(addedChar);
        }
    }
}