﻿using System;
using UnityEngine;

namespace MagesDungeon.Input
{
    [Serializable]
    public class BufferedInputBool : BufferedInputBase<bool>
    {
        [SerializeField] private bool _isFalseInputValid;
        
        protected override bool IsInputValidInternal(bool input)
        {
            return input || _isFalseInputValid;
        }
    }
}