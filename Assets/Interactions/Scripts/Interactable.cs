﻿using UnityEngine;

namespace MagesDungeon.Interactions
{
    public abstract class Interactable : MonoBehaviour
    {
        public abstract bool IsInteractable { get; }
        public abstract void Interact();
        public abstract void SetHighlightState(bool isHighlightActive);
    }
}