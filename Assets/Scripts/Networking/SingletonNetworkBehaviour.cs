﻿using Unity.Netcode;

namespace MagesDungeon.Networking
{
    public class SingletonNetworkBehaviour<T> : NetworkBehaviour
        where T : NetworkBehaviour
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this as T;
            }
            else
            {
                NetworkLog.LogError($"Instance of {typeof(T)} already exists, duplicate will be deleted");
                Destroy(this);
            }
        }
    }
}