﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterSprintModule : CharacterModuleBase
    {
        [SerializeField] private float _sprintSpeedModifier = 2;
        
        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            if (args.InputData.IsDashClicked.GetInputValue())
            {
                args.MovementDataArgs.Speed *= _sprintSpeedModifier;
            }
            return base.FixedUpdateModule(args);
        }
    }
}