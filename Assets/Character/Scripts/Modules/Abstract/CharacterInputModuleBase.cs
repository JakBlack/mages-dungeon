﻿using MagesDungeon.Character.Input;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public abstract class CharacterInputModuleBase : MonoBehaviour
    {
        public abstract CharacterInputData GetInputData();

        public abstract void Initialize();
    }
}