﻿using UnityEngine;

namespace MagesDungeon.Skills
{
    public abstract class SkillBase : ScriptableObject
    {
        public abstract SkillUseResultArgs UseSkill(SkillUseArgs args);
    }
}