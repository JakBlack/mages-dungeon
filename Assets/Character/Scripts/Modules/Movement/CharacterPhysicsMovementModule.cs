﻿using UnityEditor;
using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterPhysicsMovementModule : CharacterModuleBase
    {
        [SerializeField] private Transform _headTransform;
        [SerializeField] private Transform _bodyTransform;
        
        private Vector3 _targetVelocity;

        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            var movementDirection = args.InputData.MovementDirection;
            var isInputValid = movementDirection != default;
            args.IsInputValid = isInputValid;
            var targetVelocity = Vector3.zero;
            var rb = args.MovementDataArgs.MovementBody;

            if (isInputValid || !rb.IsSleeping())
            {
                var connectedBodyVelocity = args.ConnectedBodyVelocity;
                var velocity = GetCurrentVelocity(args);
                args.MovementDataArgs.CurrentVelocity = velocity;
                targetVelocity = GetTargetVelocity(movementDirection, args);
                var velocityDelta = GetPureVelocityDelta(targetVelocity, connectedBodyVelocity, velocity);
                velocityDelta = GetProjectedVelocityDelta(velocityDelta, args);
                velocityDelta = GetClampedVelocityDelta(velocityDelta, args.MovementDataArgs.Acceleration);

                rb.AddForce(velocityDelta, ForceMode.VelocityChange);
            }

            _targetVelocity = targetVelocity;
            
            return base.FixedUpdateModule(args);
        }
        
        private Vector3 GetTargetVelocity(Vector2 movementDirection, CharacterModuleFixedUpdateArgs args)
        {
            var touchNormal = args.TouchNormal;
            var trans = _headTransform;
            var bodyNormal = _bodyTransform.up;
            var forward = Vector3.ProjectOnPlane(trans.forward, bodyNormal).normalized;
            var right = Vector3.ProjectOnPlane(trans.right, bodyNormal).normalized;
            var relativeMovementDirection =
                movementDirection.y * forward
                + movementDirection.x * right;
            relativeMovementDirection = Vector3.ProjectOnPlane(relativeMovementDirection, touchNormal).normalized;

            return relativeMovementDirection * args.MovementDataArgs.Speed;
        }

        private static Vector3 GetCurrentVelocity(CharacterModuleFixedUpdateArgs args)
        {
            var velocity = args.MovementDataArgs.CurrentVelocity;
            if (args.IsTouchingGroundOrDidSnap && args.DidSnapToGround && !args.DidJumpRecently)
            {
                var speed = velocity.magnitude;
                velocity = Vector3.ProjectOnPlane(velocity, args.TouchNormal).normalized * speed;
                args.MovementDataArgs.MovementBody.velocity = velocity;
            }

            return velocity;
        }

        private static Vector3 GetPureVelocityDelta(Vector3 targetVelocity, Vector3 connectedBodyVelocity, Vector3 agentVelocity)
            => targetVelocity + connectedBodyVelocity - agentVelocity;

        private static Vector3 GetClampedVelocityDelta(Vector3 velocityDelta, float acceleration)
        {
            var maxVelocityDelta = acceleration * Time.fixedDeltaTime;
            
            if (velocityDelta.magnitude > maxVelocityDelta)
            {
                velocityDelta = velocityDelta.normalized * maxVelocityDelta;
            }

            return velocityDelta;
        }

        private static Vector3 GetProjectedVelocityDelta(Vector3 velocityDelta, CharacterModuleFixedUpdateArgs args)
        {
            var touchNormal = args.TouchNormal;
            
            if (!args.IsTouchingGroundOrDidSnap)
            {
                velocityDelta.y = 0;
                if (!args.IsInputValid)
                {
                    velocityDelta = default;
                }
                else if (args.IsTouchingSlope && Vector3.Dot(touchNormal, velocityDelta) < 0)
                {
                    touchNormal = new Vector3(touchNormal.x, 0, touchNormal.z).normalized;
                }
            }
            
            return Vector3.ProjectOnPlane(velocityDelta, touchNormal);
        }

        #region Gizmos

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                var center = _bodyTransform.position;
                Handles.color = Color.red;
                Handles.DrawLine(center, center + _targetVelocity * 0.1f, 2f);
            }
        }
#endif

        #endregion
    }
}
