﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public class CharacterAerialAccelerationModifierModule : CharacterModuleBase
    {
        [SerializeField] private float _aerialAccelerationMultiplier = 0.5f;
        
        public override CharacterModuleFixedUpdateArgs FixedUpdateModule(CharacterModuleFixedUpdateArgs args)
        {
            if (!args.IsTouchingGroundOrDidSnap)
            {
                args.MovementDataArgs.Acceleration *= _aerialAccelerationMultiplier;
            }
            return base.FixedUpdateModule(args);
        }
    }
}