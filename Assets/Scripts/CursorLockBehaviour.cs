﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace MagesDungeon
{
    public class CursorLockBehaviour : MonoBehaviour
    {
        [SerializeField] private CursorLockMode _defaultCursorState = CursorLockMode.Locked;
        [SerializeField] private CursorLockMode _altCursorState = CursorLockMode.None;

        private void Awake()
        {
            Cursor.lockState = _defaultCursorState;
        }

        private void SwitchCursorState()
        {
            var newState = Cursor.lockState == _defaultCursorState ? _altCursorState : _defaultCursorState;

            Cursor.lockState = newState;
        }

        public void OnCursorSwitchClicked(InputAction.CallbackContext context)
        {
            if (context.action.triggered)
            {
                SwitchCursorState();
            }
        }
    }
}