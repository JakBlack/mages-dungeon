﻿using MagesDungeon.Character.Modules;
using UnityEngine;

namespace MagesDungeon.Character
{
    public class CharacterFacadeBehaviour : MonoBehaviour
    {
        [SerializeField] private CharacterInputModuleBase _inputModule;
        [SerializeField] private CharacterModuleBase[] _initializationModules;
        [SerializeField] private CharacterModuleBase[] _lateUpdateModules;
        [SerializeField] private CharacterModuleBase[] _updateModules;
        [SerializeField] private CharacterModuleBase[] _fixedUpdateModules;

        private void Awake()
        {
            _inputModule.Initialize();
            var initializationArgs = new CharacterModuleInitializationArgs
            {
                IsOwner = true,
            };
            foreach (var playerModule in _initializationModules)
            {
                initializationArgs = playerModule.InitializeModule(initializationArgs);
            }
        }

        private void Update()
        {
            var updateArgs = new CharacterModuleUpdateArgs
            {
                InputData = _inputModule.GetInputData(),
            };

            foreach (var playerModule in _updateModules)
            {
                updateArgs = playerModule.UpdateModule(updateArgs);
            }
        }

        private void FixedUpdate()
        {
            var fixedUpdateArgs = new CharacterModuleFixedUpdateArgs
            {
                InputData = _inputModule.GetInputData(),
            };

            foreach (var playerModule in _fixedUpdateModules)
            {
                fixedUpdateArgs = playerModule.FixedUpdateModule(fixedUpdateArgs);
            }
        }

        private void LateUpdate()
        {
            var lateUpdateArgs = new CharacterModuleLateUpdateArgs
            {
                InputData = _inputModule.GetInputData(),
            };
            foreach (var playerModule in _lateUpdateModules)
            {
                lateUpdateArgs = playerModule.LateUpdateModule(lateUpdateArgs);
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Collect All Player Modules")]
        private void CollectAllPlayerModules()
        {
            _initializationModules = transform.GetComponentsInChildren<CharacterModuleBase>(true);
        }
#endif
    }
}
