﻿using UnityEngine;

namespace MagesDungeon.Character.Modules
{
    public struct CharacterMovementDataArgs
    {
        public Vector3 CurrentVelocity;
        public float Speed;
        public float Acceleration;
        public Rigidbody MovementBody;
    }
}