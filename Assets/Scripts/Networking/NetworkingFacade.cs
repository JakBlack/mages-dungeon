﻿using System;
using Cysharp.Threading.Tasks;
using MagesDungeon.Networking.Relay;
using UniRx;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

namespace MagesDungeon.Networking
{
    public class NetworkingFacade : SingletonMonoBehaviour<NetworkingFacade>
    {
        [SerializeField] private RelayManager _relayManager;
        [SerializeField] private UnityTransport _transport;
        [SerializeField] private NetworkManager _networkManager;
        [SerializeField] private ReactiveProperty<NetworkingState> _state = new();

        private readonly ReactiveProperty<RelayConnectionDataBase> _relayConnectionData = new();
        
        public IReadOnlyReactiveProperty<RelayConnectionDataBase> RelayConnectionData => _relayConnectionData;

        public IReadOnlyReactiveProperty<NetworkingState> State => _state;
        public RelayManager RelayManager => _relayManager;

        private void OnEnable()
        {
            _networkManager.OnTransportFailure += OnTransportFailure;
        }

        private void OnDisable()
        {
            _networkManager.OnTransportFailure -= OnTransportFailure;
        }

        public async void StartHost()
        {
            if (IsStateValid(NetworkingState.None))
            {
                try
                {
                    _state.Value = NetworkingState.ChangingState;
                    var connectionData = await _relayManager.HostAllocation(this.GetCancellationTokenOnDestroy());
                    _relayConnectionData.Value = connectionData;
                    if (connectionData?.IsValid == true)
                    {
                        connectionData.ApplyToTransport(_transport);
                        _state.Value = _networkManager.StartHost() ? NetworkingState.Connected : NetworkingState.None;
                    }
                    else
                    {
                        _state.Value = NetworkingState.None;
                    }
                }
                catch (Exception exception)
                {
                    _state.Value = NetworkingState.None;
                    Debug.LogException(exception);
                }
            }
        }

        public async void StartClient(string joinCode)
        {
            if (IsStateValid(NetworkingState.None))
            {
                try
                {
                    _state.Value = NetworkingState.ChangingState;
                    var joinData = await _relayManager.JoinAllocation(joinCode, this.GetCancellationTokenOnDestroy());
                    _relayConnectionData.Value = joinData;
                    if (joinData?.IsValid == true)
                    {
                        joinData.ApplyToTransport(_transport);
                        _state.Value = _networkManager.StartClient() ? NetworkingState.Connected : NetworkingState.None;
                    }
                    else
                    {
                        _state.Value = NetworkingState.None;
                    }
                }
                catch (Exception exception)
                {
                    _state.Value = NetworkingState.None;
                    Debug.LogException(exception);
                }
            }
        }

        public void Shutdown()
        {
            _networkManager.Shutdown();
            _state.Value = NetworkingState.None;
            _relayConnectionData.Value = default;
        }

        private bool IsStateValid(NetworkingState targetState)
        {
            var isStateValid = _state.Value == targetState;
            if (!isStateValid)
            {
                Debug.LogError($"State is invalid for operation, current state {_state.Value}, target state: {targetState}");
            }

            return isStateValid;
        }
        
        private void OnTransportFailure()
        {
            _state.Value = NetworkingState.None;
        }
    }
}