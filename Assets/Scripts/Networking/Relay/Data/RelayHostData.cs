﻿using Unity.Netcode.Transports.UTP;
using Unity.Services.Relay.Models;

namespace MagesDungeon.Networking.Relay
{
    public class RelayHostData : RelayConnectionDataBase
    {
        public RelayHostData(Allocation allocation, string joinCode)
            : base(allocation.ServerEndpoints, allocation.AllocationIdBytes,
                allocation.ConnectionData, allocation.Key, joinCode)
        { }

        public override void ApplyToTransport(UnityTransport transport)
        {
            transport.SetHostRelayData(Ipv4Address, Port, AllocationIdBytes, Key, ConnectionData, true);
        }
    }
}