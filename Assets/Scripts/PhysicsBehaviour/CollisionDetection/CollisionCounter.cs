﻿using MagesDungeon.Extensions;
using UnityEngine;

namespace MagesDungeon.PhysicsBehaviour
{
    public class CollisionCounter : MonoBehaviour
    {
        [SerializeField, Range(0, 90)] private float _maxGroundAngle = 45f;
        [SerializeField] private Rigidbody _parentBody;

        public Vector3 GroundNormal => _groundNormalsSum.normalized;
        public Vector3 ConnectedBodyVelocity { get; private set; }
        public Vector3 SlopeNormal => _slopeNormalsSum.normalized;

        
        private Vector3 _groundNormalsSum;
        private Vector3 _slopeNormalsSum;

        private void FixedUpdate()
        {
            _groundNormalsSum = default;
            _slopeNormalsSum = default;
            ConnectedBodyVelocity = default;
        }

        private void OnCollisionEnter(Collision collision)
        {
            ValidateCollision(collision);
        }
        
        private void OnCollisionStay(Collision collision)
        {
            ValidateCollision(collision);
        }

        private void ValidateCollision(Collision collision)
        {
            foreach (var contactPoint in collision.contacts)
            {
                var normal = contactPoint.normal;

                if (IsCollisionNormalValid(normal))
                {
                    _groundNormalsSum += normal;
                    ConnectedBodyVelocity += collision.rigidbody.GetConnectedBodyVelocity(_parentBody);
                }
                else if (normal.y > -0.001f)
                {
                    _slopeNormalsSum += normal;
                }
            }
        }

        public bool IsCollisionNormalValid(Vector3 normal)
        {
            var upDotProduct = Vector3.Dot(Vector3.up, normal);
            return upDotProduct >= Mathf.Cos(_maxGroundAngle * Mathf.Deg2Rad);
        }
    }
}